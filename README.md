<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url] -->
<!-- [![Forks][forks-shield]][forks-url] -->
<!-- [![Stargazers][stars-shield]][stars-url] -->
<!-- [![Issues][issues-shield]][issues-url] -->
<!-- [![MIT License][license-shield]][license-url] -->
<!-- [![LinkedIn][linkedin-shield]](https://www.linkedin.com/in/blackhiden/) -->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/blackhiden/docker-installer">
    <img src="https://www.docker.com/wp-content/uploads/2022/03/horizontal-logo-monochromatic-white.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Docker Installer</h3>

  <p align="center">
    This script will help Debian and Ubuntu user to install docker in one-line command, including test the machine if they support docker or not by run hello-world container. 
    <br />
    <a href="https://gitlab.com/blackhiden/docker-installer"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <!-- <a href="https://github.com/github_username/repo_name">View Demo</a> -->
    <a href="https://gitlab.com/blackhiden/docker-installer/issues">Report Bug</a>
    <a href="https://gitlab.com/blackhiden/docker-installer/issues">Request Feature</a>
  </p>
</div>

<!-- Please Enable or Disable readme here. I'm just too lazy to finish README.md -->

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
    <li><a href="#thanks-to">Thanks to</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

This project was made to simplify the installation docker engine instead of install the packages one-by-one.

<!-- Here's a blank template to get started: To avoid retyping too much info. Do a search and replace with your text editor for the following: `github_username`, `repo_name`, `twitter_handle`, `linkedin_username`, `email_client`, `email`, `project_title`, `project_description` -->

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

- [Bash](https://www.gnu.org/software/bash/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

This scripts work perfectly fine (and only) under debian distribution (tested on Debian Buster 10 and Ubuntu Focal Fossa 20.04). Inside install.sh is just regular bash script that run apt either apt update and apt install of a collection of docker package along with it's dependencies.

### Prerequisites

Make sure you have the following packages installed:

- curl
  ```sh
  apt update && apt install -y curl
  ```

In minimal distro, they don't come with curl, so maybe you need to install it manually

- Of course you need full `root` access because these scripts never work with sudo. I prohibit sudo usage in under some circumstances

### Installation

1. First thing first, switch user into root: run the following command as root (without sudo):

   ```sh
   sudo su -
   ```

2. Then run the following command:

   ```sh
   bash - <(curl -fsSL https://script.blackhiden.com/docker-installer/install.sh)
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

<p align="right">(<a href="#top">back to top</a>)</p>

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Twitter: [@blackhiden](https://twitter.com/blackhiden)

Facebook: [Denny Lastpaste](https://facebook.com/blackhiden)

Instagram: [@blackhiden](https://instagram.com/blackhiden)

Email: [blackhiden@gmail.com](mailto:blackhiden@gmail.com)

Blog: [Everything - It's all about me, computer and my life](https://blackhiden.blogspot.com/)

Project Link: [https://gitlab.com/blackhiden/openvpn-script](https://gitlab.com/blackhiden/openvpn-script)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

<!-- * []() -->

<p align="right">(<a href="#top">back to top</a>)</p>

## Thanks to

I'm so dumb that I have to copy this amazing README.md template. Thanks to [Othneil Drew](https://github.com/othneildrew/Best-README-Template)

<!-- * []() -->

<p align="right">(<a href="#top">back to top</a>)</p>
