#!/bin/bash
clear
UBLUE='\033[4;34m'
BBLUE='\033[1;34m'
BRED='\033[1;31m'
BGREEN='\033[1;32m'
BBLACK='\033[1;30m'
NC='\033[0m' # No Color
spin(){
    stringSpin="$@"
    spinner=(⣾ ⣽ ⣻ ⢿ ⡿ ⣟ ⣯ ⣷ )
    tput civis
    while :
    do
        for frame in "${spinner[@]}"
        do
        #   echo -n "${spinner:$i:1}"
        printf "\r%s" "[ ${frame} ] $stringSpin "
        sleep 0.05
        done
    done
}

startSpin(){
    spin "$@" &
    spinpid=$!
}

stopSpinSuccess(){
    echo -en "\033[2K"
    kill $spinpid &> /dev/null
    printf "\r[ ${BGREEN}✔${NC} ] $@\n"
    tput cnorm
}

startSubSpin(){
    echo -en "\033[2K"
    kill $spinpid &> /dev/null
    spin "$@" &
    spinpid=$!
}

stopSpinFailed(){
    echo -en "\033[2K"
    kill $spinpid &> /dev/null
    printf "\r[ ${BRED}⨉${NC} ] $@\n"
    tput cnorm
}

printf "
┌--------------------------------------------------------------------------------┐
|                       ${BBLUE}${UBLUE}Docker Installer by Denny Lastpaste${NC}                      |
|                            This script will install                            |
|                          all docker package and verify                         |
|                          if your machine is supported                          |
|                                by docker engine                                |
|                                     Usage:                                     | 
| bash - <(curl -fsSL https://script.blackhiden.com/docker-installer/install.sh) |
|                          ${BRED}NOTE: Debian and Ubuntu only${NC}                          |
└--------------------------------------------------------------------------------┘
"
# Preparing
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DOCKER_LOG=$(pwd)/docker-install.log
echo "[$(date)][Docker Install]" >> "$DOCKER_LOG"

# Checking requirements
# ========================================================================================================
startSpin "Checking requirements"
sleep 2
if [[ $EUID -ne 0 ]] || [[ ! -z $SUDO_USER ]] ;
then
	echo "You're not root. This script should be run as root without sudo in order to work properly " >> "$DOCKER_LOG"
	stopSpinFailed "Update failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
    exit 1
fi

version=$(grep '^NAME' /etc/os-release)
version=${version,,}
ubuntu='Ubuntu'
debian='Debian'

if [[ "$version" != *"debian"* ]] && [[ "$version" != *"ubuntu"* ]]
then
	echo "This script only works to Debian or Ubuntu OS" >> "$DOCKER_LOG"
	stopSpinFailed "Update failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
	exit 1
fi
stopSpinSuccess "Your system meet requirement"

# Begin installation
# ========================================================================================================
startSpin "Update repository"
apt-get clean
apt-get autoclean > /dev/null
apt-get update > /dev/null 2>> "$DOCKER_LOG"
if [ "$?" -ne 0 ] ; then
	stopSpinFailed "Update failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
	exit 1
fi
stopSpinSuccess "Update success"

startSpin "Installing all support package"
apt-get install -y ca-certificates curl gnupg lsb-release > /dev/null 2>> "$DOCKER_LOG"
if [ "$?" -ne 0 ] ; then
	stopSpinFailed "Install failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
	exit 1
fi
[ -d /etc/apt/keyrings ] && rm -rf /etc/apt/keyrings
mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg 2>> "$DOCKER_LOG"
if [ "$?" -ne 0 ] ; then
	stopSpinFailed "Process failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
	exit 1
fi
chmod a+r /etc/apt/keyrings/docker.gpg
[ -f /etc/apt/sources.list.d/docker.list ] && rm -rf /etc/apt/sources.list.d/docker.list
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update > /dev/null 2>> "$DOCKER_LOG"
stopSpinSuccess "Support package installed successfully"

startSpin "Installing main package (Please wait, This process will depend on your internet speed)"
apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-compose > /dev/null 2>> "$DOCKER_LOG"
if [ "$?" -ne 0 ] ; then
	stopSpinFailed "Install failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
	exit 1
fi
stopSpinSuccess "Main package installed successfully"

startSpin "Pulling dan testing 'hello-world' image to ensure that your system can run docker"
docker run hello-world > /dev/null 2>&1
if [ "$?" -ne 0 ] ; then
	stopSpinFailed "Update failed. Please see error on "$DOCKER_LOG""
	printf "FAILED\n\n" >> "$DOCKER_LOG"
	exit 1
fi
stopSpinSuccess "Your system can run docker"

printf "${BGREEN}Congratulation! You have docker system installed${NC}\n"

# Final
printf "SUCCESS\n\n" >> "$DOCKER_LOG"
