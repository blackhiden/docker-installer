# Changelog

## v1.0.1

### ADDED

- CHANGELOG.md

### UPDATED

- Typo in readme

### REMOVED

- Nothing removed

## v1.0.0

### ADDED

- main features
